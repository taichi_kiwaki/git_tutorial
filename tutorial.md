# グループ分け
近くの人で集まって，２ー３人のグループを作ってください．また各人に0-2の番号を割り当ててください．

# 情報登録と初期化
まずターミナルを開き，e-mailアドレスと名前を編集後以下を実行

``` bash
git config --global user.email "your_address@some.domabin.com"
git config --global user.name "Your Name"
git config --global alias.tree "log --oneline --decorate --all --graph"
```

# リモートリポジトリの作成
**番号0の人**は，bitbucket上に"dss_git_tutorial"というリポジトリを作成してください．
![init_repo](images/init_repo.png)

他のグループメンバーを，そのリポジトリにdevelopperとして追加してください．（追加法は[tutorial_dss19s](https://data-science-school.slack.com/files/U5193KC92/FJ4NN4XSR/tutorial_dss19s.pdf)）を参照のこと．

# クローン
**"account_name"を番号0番目の人のアカウントに正しく修正して**（もしくはブラウザからコマンド文字列をコピーして），以下を実行
``` bash
git clone git@bitbucket.org:account_name/dss_git_tutorial.git
```
この作業でlocal repositoryとして`~/dss_git_tutorial/`というディレクトリが作成されている．

``` bash
ls -a dss_git_tutorial/
```
を実行すると`.git`というディレクトリが見えるはずである．以下ではlocal repository内で作業をするため

``` bash
cd dss_git_tutorial/
```
を実行．


# はじめてのコミット（*この作業は番号0の人のみ実行．他の人は作業を眺めておくこと*）
**番号0の人**は，以下の内容で"readme.md"を作成．
``` markdown
# Section 0
Section 0 text
# Section 1
Section 1 text
# Section 2
Section 2 text
# Appendix
Here is appendix
```

次に以下を実行．
``` bash
#Have git to manage versions of the file.
git add readme.md
#Make a first commit with a commit message
git commit -m 'Initialized readme.md' readme.md
```

この結果，
``` bash
[master (root-commit) 535660a] Initialized readme.md
 1 file changed, 1 insertion(+)
 create mode 100644 readme.md
```
などと表示されれば成功．これでlocal repositoryには自動的にmasterというブランチが作成されている．

## 注意
この時点ではまだ0番目の人のlocal repositoryにしか作業が登録されていないことに注意．
他の番号のひとは，あたり前であるが自分のlocal repositoryにreadme.mdが無いことを確認してみても良い．
また次で紹介する`git pull origin master`を**この時点で**実行しても`fatal: couldn't find remote ref master`などとErrorが出る．

# push/pullによる成果の共有
**番号0の人**は次を実行
``` bash
git push origin master
```
この結果
``` bash
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 295 bytes | 295.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To bitbucket.org:taichi_kiwaki/foobar.git
 * [new branch]      master -> master
```
などと表示されれば成功．originやmasterに関してはpdfの配布資料を参照のこと．この時点でremote repositoryに成果が登録される．Bitbucketでrepositoryを確認すれば，readme.mdが作成されていることが確認できるはず．

これを受けて，**0番以外の人**は
``` bash
git pull origin master
```
を実行．

``` bash
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From bitbucket.org:taichi_kiwaki/foobar
 * branch            master     -> FETCH_HEAD
 * [new branch]      master     -> origin/master
```
などと表示されれば成功．local repositoryにreadme.mdが作成されているはず．

# 作業のマージ
i番目の人はreadme.mdのSection iのテキストを自由に編集し，適当なコメントを付けて`git commit`を行ってください．**自分の番号以外のSectionは決して編集しないこと!**

この作業後に各人，
```bash
git push origin master
```
を実行．

もし
``` bash
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'git@bitbucket.org:taichi_kiwaki/foobar.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```
などと表示されたら，それは他の人が既にCommitをpushしている．その場合は
``` bash
git pull origin master
```
を行ってから，再度pushを実行する．

最後の人がpushを終えたら，それ以外の人はもう一度
``` bash
git pull origin master
```
を実行する．
**これによって他の人の編集したテキストがreadme.mdに反映されていることを確認すること．この様に複数人が同時に行った作業が統合されるのはGitのマージ機能のおかげである．**

# コンフリクトとその解消
Gitのマージ機能は便利であるが限界も大きい．例えば複数人がファイルの同じ箇所を編集してしまった場合，統合に失敗してしまう．この場合は手動で統合を行うこととなる．ここではその場合の対処法を学ぼう．

全ての人が**Section 0**のテキストを編集してください．テキストが複数行ある場合は全ての行を改変してください．
次に，0番の人は`commit`と`push`を実行してください．

以下は1番目の人から**割り振られた番号順**に実行すること．`commit`を行い自分の作業を登録したのち，
``` bash
git pull origin master
```
を実行してください．その結果，
``` bash
From bitbucket.org:taichi_kiwaki/foobar
 * branch            master     -> FETCH_HEAD
Auto-merging readme.md
CONFLICT (content): Merge conflict in readme.md
Automatic merge failed; fix conflicts and then commit the result.
```
と，Gitがマージに失敗した旨が報告されます．実際，readme.mdはマージに失敗したSection 0で
``` text
<<<<<<< HEAD
# You say yes, I say no
=======
# This is section 2 text that looks awesome
>>>>>>> dbc8ce3d476481964ebdb9d6bd087a6d7f3de86c
```
様になっているはずです．この`<<<<<<< HEAD`や`=======`を取り除き，ファイルを編集してください．
**次に`-i`オプションを付けて，commitを行います：**

``` bash
git commit -im 'Resolved conflict' .
```
結果として
``` bash
[master d1a10f3] Resolved conflict
```
などと表示されればマージは成功なので，`push`を行ったのち次の番号に人にバトンタッチしてください．
